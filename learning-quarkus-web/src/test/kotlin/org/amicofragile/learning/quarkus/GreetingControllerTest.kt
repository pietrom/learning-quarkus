package org.amicofragile.learning.quarkus

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import org.hamcrest.CoreMatchers.equalTo
import org.junit.jupiter.api.Test

@QuarkusTest
class GreetingControllerTest {
    @Test
    fun testHelloEndpoint() {
        given()
                .`when`().get("/greeting")
                .then()
                .statusCode(200)
                .and()
                .body("greeting", equalTo("Hello, World!"))
    }
}