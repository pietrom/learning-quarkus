package org.amicofragile.learning.quarkus

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/greeting")
class GreetingController(private val provider: MessageProviderService) {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun hello() = HelloData(provider.message(), "World")
}

data class HelloData(val message: String, val to: String) {
    val greeting: String
        get() = "$message, $to!"
}

interface MessageProviderService {
    fun message() : String
}

class ClassicMessageProviderService : MessageProviderService {
    override fun message(): String = "Hello"
}