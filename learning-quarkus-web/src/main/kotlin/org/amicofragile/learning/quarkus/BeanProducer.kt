package org.amicofragile.learning.quarkus

import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Produces

@ApplicationScoped
open class BeanProducer {
    @Produces
    fun messageProvider(): MessageProviderService = ClassicMessageProviderService()
}